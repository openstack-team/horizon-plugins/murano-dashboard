Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: murano-dashboard
Source: https://github.com/openstack/murano-dashboard.git

Files: *
Copyright: (c) 2013, Hewlett-Packard Development Company, L.P.
           (c) 2013-2016, Mirantis, Inc.
           (c) 2013, IBM Corp.
           (c) 2010-2016, OpenStack Foundation
           (c) 2010, US Government as represented by the Administratof of NASA
           (c) 2016, AT&T Corp
           (c) 2015, ThoughtWorks Inc.
License: Apache-2.0

Files: debian/*
Copyright: (c) 2014-2018, Thomas Goirand <zigo@debian.org>
           (c) 2019, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2.0

Files: tools/rfc.sh
Copyright: (c) 2010-2011 Gluster, Inc. <http://www.gluster.com>
License: GPLv3-gluster
 This initial version of this file was taken from the source tree of GlusterFS.
 It was not directly attributed, but is assumed to be Copyright (c) 2010-2011
 Gluster, Inc and release GPLv3 Subsequent modifications are Copyright (c) 2012
 OpenStack, LLC.
 .
 GlusterFS is free software; you can redistribute it and/or modify it under the
 terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.
 .
 GlusterFS is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License v2
 (GPL) can be found in /usr/share/common-licenses/GPL-3.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".
